import { Album } from "src/model/Album";
import { Reducer, Action, ActionCreator } from "redux";

export type SearchState = {
  results: Album[];
  query: string;
  loading: boolean;
  error?: string;
};

const initialState: SearchState = {
  results: [],
  query: "",
  loading: false
};

export const search: Reducer<SearchState, ActionTypes> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case "SEARCH_START":
      return { ...state, loading: true, error: "", query: action.payload };
    case "SEARCH_SUCCESS":
      return { ...state, loading: false, results: action.payload };
    case "SEARCH_FAILED":
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};

type ActionTypes = SEARCH_START | SEARCH_SUCCESS | SEARCH_FAILED;

interface SEARCH_START extends Action<"SEARCH_START"> {
  payload: string;
}
interface SEARCH_SUCCESS extends Action<"SEARCH_SUCCESS"> {
  payload: Album[];
}
interface SEARCH_FAILED extends Action<"SEARCH_FAILED"> {
  payload: string;
}

export const searchStart: ActionCreator<SEARCH_START> = (payload: string) => ({
  type: "SEARCH_START",
  payload
});

export const searchSuccess: ActionCreator<SEARCH_SUCCESS> = (
  payload: Album[]
) => ({
  type: "SEARCH_SUCCESS",
  payload
});

export const searchFailed: ActionCreator<SEARCH_FAILED> = (
  payload: string
) => ({
  type: "SEARCH_FAILED",
  payload
});
