import { Security } from "./Security";
import { AlbumsResponse, Album } from "../model/Album";
export class Search {
  api_url = "https://api.spotify.com/v1/search";
  results: Album[] = [];

  constructor(private security: Security) {}

  search(query: string) {
    const url = `${this.api_url}?type=album&q=${query}`;

    return fetch(url, {
      headers: {
        Authorization: "Bearer " + this.security.getToken()
      }
    })
      .then<AlbumsResponse>(resp => {
        if (!resp.ok) {
          if (resp.status == 401) {
            this.security.authorize();
          }

          return resp.json().then(err => {
            return Promise.reject(err.error.message);
          });
        }
        return resp.json();
      })
      .then(resp => resp.albums.items)
      .then(results => {
        this.results = results;
        return results;
      });
  }
}
